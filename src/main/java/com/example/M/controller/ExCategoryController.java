package com.example.M.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.M.entity.Category;
import com.example.M.entity.Expense;
import com.example.M.repository.CategoryRepository;
import com.example.M.repository.ExpenseRepository;
import com.example.M.repository.UserRepository;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
@RequestMapping("/expense/category")
public class ExCategoryController {
	@Autowired
	CategoryRepository categoryRepository;
	@Autowired
	ExpenseRepository expenseRepository;
	@Autowired
	UserRepository userRepository;

	// カテゴリと紐づいた支出の削除
	@CrossOrigin
	@DeleteMapping("/delete/{categoryId}/{userId}")
	public List<Category> delete(@PathVariable("categoryId") Integer categoryId, @PathVariable("userId") Integer userId) {

		List<Expense> expenses = expenseRepository.findAllByUserIdAndCategoryId(userId, categoryId);
		for(Expense expense : expenses) {
			expenseRepository.deleteById(expense.getId());
		}

		// カテゴリの削除
		categoryRepository.deleteById(categoryId);
		// ユーザーの支出情報を取得して返す
		return categoryRepository.findAllByUserId(userId);
	}


	// カテゴリの追加
	@CrossOrigin
	@PostMapping("/add")
	public List<Category> add(@RequestBody Category category) {
		categoryRepository.save(category);

		return categoryRepository.findAllByUserId(category.getUserId());
	}

	// カテゴリの編集
	@CrossOrigin
	@PutMapping("/edit/{id}")
	public List<Category> edit(@PathVariable("id") Integer id,@RequestBody Category category) {
		Category c = categoryRepository.save(category);
		List<Category> categories = categoryRepository.findAllByUserId(id);
		return categories;
	}

	@CrossOrigin
	@GetMapping("/{id}")
	public List<Category> getAll(@PathVariable("id") Integer userId) throws JsonProcessingException {
		List<Category> categories = categoryRepository.findAllByUserId(userId);
		return categories;
	}

}
