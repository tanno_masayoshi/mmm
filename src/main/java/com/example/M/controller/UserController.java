package com.example.M.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.M.entity.Category;
import com.example.M.entity.Expense;
import com.example.M.entity.Income;
import com.example.M.entity.User;
import com.example.M.repository.CategoryRepository;
import com.example.M.repository.ExpenseRepository;
import com.example.M.repository.IncomeRepository;
import com.example.M.repository.UserRepository;
import com.example.M.utils.CipherUtil;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
@RequestMapping("/users")
public class UserController {
	private final UserRepository userRepository;
	private final CategoryRepository categoryRepository;
	private final ExpenseRepository expenseRepository;
	private final IncomeRepository incomeRepository;

	public UserController(UserRepository userRepository, CategoryRepository categoryRepository, ExpenseRepository expenseRepository, IncomeRepository incomeRepository) {
		this.userRepository = userRepository;
		this.categoryRepository = categoryRepository;
		this.expenseRepository = expenseRepository;
		this.incomeRepository = incomeRepository;
	}

	// 重複バリデーション用
	@CrossOrigin
	@GetMapping
	public ResponseEntity<List<User>> getAll() throws JsonProcessingException {
		List<User> users = userRepository.findAllIdAndAccount();

		return ResponseEntity.ok().header("ContentType", MediaType.APPLICATION_JSON_VALUE.toString()).body(users);
	}

	// ユーザー登録
	@CrossOrigin
	@PostMapping("signup")
	public User save(@RequestBody User request) {

		// パスワード暗号化
		request.setPassword(CipherUtil.encrypt(request.getPassword()));

		User user = userRepository.save(request);

		// デフォルト用のカテゴリの追加
		int userId = user.getId();
		List<Category> categories = new ArrayList<>();
		String[] names = {"食費", "日用品", "衣服", "医療費", "交際費", "光熱費", "交通費", "通信費", "居住費"};


		for(String name : names) {
			Category category = new Category();
			category.setUserId(userId);
			category.setName(name);
			categories.add(category);
		}

		for(Category c : categories) {
			categoryRepository.save(c);
		}


		return user;
	}

	// ログイン
	@CrossOrigin
	@PostMapping("login")
	public User login(@RequestBody User reqUser) {

		// パスワード暗号化
		reqUser.setPassword(CipherUtil.encrypt(reqUser.getPassword()));

		User user = userRepository.login(reqUser.getAccount(), reqUser.getPassword());

		user.setPassword(null);

		return user;
	}

	// 更新
	@CrossOrigin
	@PutMapping("edit/{id}")
	public ResponseEntity<User> edit(@PathVariable("id") Integer id, @RequestBody User user) {
		User currentUser = userRepository.getOne(id);
		// パスワードが空だったら、元のパスワードを使用する
		if(StringUtils.isEmpty(user.getPassword())) {
			user.setPassword(currentUser.getPassword());
		} else {
			// パスワード暗号化
			user.setPassword(CipherUtil.encrypt(user.getPassword()));
		}

		User u = userRepository.save(user);
		return ResponseEntity.ok().header("ContentType", MediaType.APPLICATION_JSON_VALUE.toString()).body(user);
	}

	// 削除
	@CrossOrigin
	@DeleteMapping("/delete/{id}")
	public void delete(@PathVariable("id") Integer id) {
		// カテゴリ検索
		List<Category> categories = categoryRepository.findAllByUserId(id);
		// 支出検索
		List<Expense> expenses = expenseRepository.findAllByUserId(id);
		// 収入検索
		List<Income> incomes = incomeRepository.findByUserId(id);
		userRepository.deleteById(id);
		// カテゴリ削除
		for(Category category: categories) {
			categoryRepository.deleteById(category.getId());
		}
		// 支出削除
		for(Expense expense : expenses) {
			expenseRepository.deleteById(expense.getId());
		}
		// 収入削除
		for(Income income: incomes) {
			incomeRepository.deleteById(income.getId());
		}
	}

}
