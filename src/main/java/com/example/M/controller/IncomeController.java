package com.example.M.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.M.entity.Income;
import com.example.M.repository.IncomeRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/incomes")
public class IncomeController {
	@Autowired
	IncomeRepository incomeRepository;

	@CrossOrigin
	@GetMapping("/getall/{userId}")
	public List<Income> getAllIncome(@PathVariable("userId") Integer userId) throws IOException {
		return incomeRepository.findByUserId(userId);
		//ResponseEntity.ok().header("ContentType", MediaType.APPLICATION_JSON_VALUE.toString()).body(incomes);
	}

	@CrossOrigin
	@GetMapping("/getall/{userId}/{startDate}/{endDate}")
	public List<Income> getIncomeByManth(@PathVariable("userId") Integer userId, @PathVariable("startDate") String startDate, @PathVariable("endDate") String endDate ) throws IOException, ParseException {
		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date sDate = sdFormat.parse(startDate);
		Date eDate = sdFormat.parse(endDate);
		return incomeRepository.findByUserIdAndIncomeDateBetweenOrderByIncomeDateDesc(userId, sDate, eDate);
		//ResponseEntity.ok().header("ContentType", MediaType.APPLICATION_JSON_VALUE.toString()).body(incomes);
	}
	@CrossOrigin
	@GetMapping(value="/get/{id}")
	public ResponseEntity<String> getIncome(@PathVariable Integer id) throws JsonProcessingException {
		Income income = incomeRepository.findById(id).orElse(null);
		ObjectMapper mapper = new ObjectMapper();
		String jsonStr = mapper.writeValueAsString(income);
		return ResponseEntity.ok().header("ContentType", MediaType.APPLICATION_JSON_VALUE.toString()).body(jsonStr);
	}

	@CrossOrigin
	@ResponseBody
	@PostMapping("/add")
	public ResponseEntity<List<Income>> addIncome(@RequestBody String json) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(json);
		Integer amount = node.get("amount").asInt();
		Date incomeDate = java.sql.Date.valueOf(node.get("income_date").textValue());
		String category = node.get("category").textValue();
		Integer userId = node.get("user_id").asInt();
		//entityincomeへset
		Income income = new Income();
		income.setAmount(amount);
		income.setIncomeDate(incomeDate);
		income.setCategory(category);
		income.setUserId(userId);
		incomeRepository.save(income);

		List<Income> incomes = incomeRepository.findByUserId(userId);
		return ResponseEntity.ok().header("ContentType", MediaType.APPLICATION_JSON_VALUE.toString()).body(incomes);
	}

	//updateはidを含めて受け取りを行う
	@CrossOrigin
	@ResponseBody
	@PostMapping("/update")
	public void updateIncome(@RequestBody Income income) throws IOException {
		incomeRepository.save(income);
	}

	@CrossOrigin
	@ResponseBody
	@DeleteMapping("/delete/id={id}&user={userId}")
	public ResponseEntity<List<Income>> deleteIncome(@PathVariable("id") Integer id, @PathVariable("userId") Integer userId) {
		incomeRepository.deleteById(id);
		List<Income> incomes = incomeRepository.findByUserId(userId);
		return ResponseEntity.ok().header("ContentType", MediaType.APPLICATION_JSON_VALUE.toString()).body(incomes);
	}
}
