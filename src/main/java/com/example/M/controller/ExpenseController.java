package com.example.M.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.M.entity.Category;
import com.example.M.entity.Expense;
import com.example.M.repository.CategoryRepository;
import com.example.M.repository.ExpenseRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("expense")
public class ExpenseController {

	@Autowired
	ExpenseRepository expenseRepository;
	@Autowired
	CategoryRepository categoryRepository;

	@CrossOrigin
	@ResponseBody
	@PostMapping(value="/add")
	public void addExpence(@RequestBody Expense expense) throws JsonMappingException, JsonProcessingException {
		expenseRepository.save(expense);
	}

	@CrossOrigin
	@ResponseBody
	@GetMapping(value="/getall/{userId}/{start}/{end}")
	public ResponseEntity<String> getAllExpenses(@PathVariable Integer userId,
			@PathVariable String start, @PathVariable String end) throws JsonProcessingException, ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date dateStart = dateFormat.parse(start);
		Date dateEnd = dateFormat.parse(end);
		List<Expense> expenseList = expenseRepository.findAllByUserId(userId, dateStart, dateEnd);
		ObjectMapper mapper = new ObjectMapper();
		String jsonStr = mapper.writeValueAsString(expenseList);
		return ResponseEntity.ok().header("ContentType", MediaType.APPLICATION_JSON_VALUE.toString()).body(jsonStr);
	}

	@CrossOrigin
	@ResponseBody
	@GetMapping(value="/getcategory")
	public ResponseEntity<String> getCategory() throws JsonProcessingException {
		List<Category> categoryList = categoryRepository.findAll();
		ObjectMapper mapper = new ObjectMapper();
		String jsonStr = mapper.writeValueAsString(categoryList);
		return ResponseEntity.ok().header("ContentType", MediaType.APPLICATION_JSON_VALUE.toString()).body(jsonStr);
	}

	@CrossOrigin
	@GetMapping(value="/get/{id}")
	public ResponseEntity<String> getExpense(@PathVariable Integer id) throws JsonProcessingException {
		Expense expense = expenseRepository.findById(id).orElse(null);
		ObjectMapper mapper = new ObjectMapper();
		String jsonStr = mapper.writeValueAsString(expense);
		return ResponseEntity.ok().header("ContentType", MediaType.APPLICATION_JSON_VALUE.toString()).body(jsonStr);
	}


	@CrossOrigin
	@DeleteMapping(value="/delete/{id}")
	public void deleteExpense(@PathVariable Integer id) {
		expenseRepository.deleteById(id);
	}
}