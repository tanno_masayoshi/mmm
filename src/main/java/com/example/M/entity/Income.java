package com.example.M.entity;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "incomes")
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Getter
@Setter
public class Income {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;
    @Column(name="amount")
    private Integer amount;
    @Column(name = "income_date")
    private Date incomeDate;
    @Column(name = "category")
    private String category;
    @Column(name = "userId")
    private Integer userId;
}
