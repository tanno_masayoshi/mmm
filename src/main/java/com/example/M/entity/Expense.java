package com.example.M.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Configuration
@Data
@Entity
@Table(name = "expenses")
public class Expense {
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name="category_id")
	private int categoryId;
	@Column(name="user_id")
	private int userId;
	@Column
	private String description;
	@Column
	private int price;
	@Column(name="buy_date")
	private Date buyDate;

	@ManyToOne
    @JoinColumn(insertable=false, updatable=false, name="category_id")
    private Category category;
}
