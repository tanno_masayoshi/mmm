package com.example.M.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.M.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

	@Query("select u from User u where u.account = :account and u.password = :password ")
	User login(@Param("account") String account, @Param("password") String password);

	@Query("select u from User u")
	List<User> findAllIdAndAccount();
}
