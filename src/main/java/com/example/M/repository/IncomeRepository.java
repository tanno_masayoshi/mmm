package com.example.M.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.M.entity.Income;

@Repository
public interface IncomeRepository extends JpaRepository<Income, Integer> {

	List<Income> findByUserId(Integer userId);

	List<Income> findByUserIdAndIncomeDateBetweenOrderByIncomeDateDesc(Integer userId, Date startDate, Date endDate);

}
