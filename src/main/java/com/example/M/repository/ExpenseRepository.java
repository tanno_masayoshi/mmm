package com.example.M.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.M.entity.Expense;

@Repository
public interface ExpenseRepository extends JpaRepository<Expense, Integer> {

	@Query("SELECT e FROM Expense e INNER JOIN e.category "
			+ "WHERE e.userId = :userId AND e.buyDate BETWEEN :start AND :end ORDER BY e.buyDate")
    List<Expense> findAllByUserId(@Param("userId") Integer userId,
    		@Param("start") Date start, @Param("end") Date end);

	@Query("delete from Expense e Where e.userId = :userId AND e.categoryId = :categoryId")
	void deleteAllByUserIdAndCategoryId(@Param("userId") Integer userId, @Param("categoryId") Integer categoryId);

	@Query("select e from Expense e Where e.userId = :userId AND e.categoryId = :categoryId")
	List<Expense> findAllByUserIdAndCategoryId(@Param("userId") Integer userId, @Param("categoryId") Integer categoryId);

	@Query("select e from Expense e where e.userId = :userId")
	List<Expense> findAllByUserId(@Param("userId") Integer userId);
}
