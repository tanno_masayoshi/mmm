create table users (
id SERIAL NOT NULL,
account VARCHAR(20) NOT NULL UNIQUE,
name VARCHAR(10) NOT NULL,
password VARCHAR(255) NOT NULL,
reference_date Date NOT NULL,
PRIMARY KEY (id)
);

CREATE TABLE incomes (
id SERIAL NOT NULL,
amount INTEGER NOT NULL,
income_date DATE NOT NULL,
category VARCHAR(10) NOT NULL,
user_id INTEGER NOT NULL,
PRIMARY KEY(id)
);

create table categories (
id SERIAL NOT NULL,
name VARCHAR(10) NOT NULL,
user_id Integer NOT NULL,
PRIMARY KEY (id)
);

CREATE TABLE expenses (
id SERIAL NOT NULL,
description VARCHAR,
buy_date DATE NOT NULL,
price INTEGER NOT NULL,
category_id INTEGER NOT NULL,
user_id INTEGER NOT NULL,
PRIMARY KEY(id)
);
